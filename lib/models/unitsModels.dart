import 'package:cloud_firestore/cloud_firestore.dart';

class UnitsModels {
  String? type;
  String? brand;
  bool? available;
  String? displacement;
  String? gear;
  String? pic;
  String? rating;
  int? price;
  String? released;

  UnitsModels();

  Map<String, dynamic> toJson() => {
        'type': type,
        'brand': brand,
        'available': available,
        'displacement': displacement,
        'gear': gear,
        'pic': pic,
        'rating': rating,
        'price': price,
        'released': released
      };

  UnitsModels.fromSnapshot(snapshot)
      : type = snapshot.data()['type'],
        brand = snapshot.data()['brand'],
        available = snapshot.data()['available'],
        displacement = snapshot.data()['displacement'],
        gear = snapshot.data()['gear'],
        pic = snapshot.data()['pic'],
        rating = snapshot.data()['rating'],
        price = snapshot.data()['price'],
        released = snapshot.data()['released'];
}
