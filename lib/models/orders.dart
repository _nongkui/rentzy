import 'package:cloud_firestore/cloud_firestore.dart';

class Orders {
  String? status;
  String? startDate;
  String? endDate;
  String? type;
  String? brand;
  String? pic;
  String? method;
  int? price;
  int? totalPrice;
  bool? confirmed;
  String? userId;

  Orders();

  Map<String, dynamic> toJson() => {
        'status': status,
        'startDate': startDate,
        'endDate': endDate,
        'type': type,
        'brand': brand,
        'pic': pic,
        'method': method,
        'price': price,
        'totalPrice': totalPrice,
        'confirmed': confirmed,
        'userId': userId,
      };

  Orders.fromSnapshot(snapshot)
      : status = snapshot.data()['status'],
        startDate = snapshot.data()['startDate'],
        endDate = snapshot.data()['endDate'],
        type = snapshot.data()['type'],
        brand = snapshot.data()['brand'],
        pic = snapshot.data()['pic'],
        method = snapshot.data()['method'],
        price = snapshot.data()['price'],
        totalPrice = snapshot.data()['totalPrice'],
        confirmed = snapshot.data()['confirmed'],
        userId = snapshot.data()['userId'];
}
