// ignore_for_file: prefer_interpolation_to_compose_strings

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/models/orders.dart';
import 'package:rentzy_rpl/pages/invoice.dart';
import 'package:rentzy_rpl/pages/unitRetrieval.dart';

class historyCard extends StatelessWidget {
  final Orders orders;

  historyCard(this.orders);

  rentDate() {
    var startDate = orders.startDate.toString().substring(8, 10);
    var endDate = orders.endDate.toString().substring(8, 10);

    var rentDate = startDate + '-' + endDate;
    return rentDate;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: GestureDetector(
        onTap: () {
          if (orders.status == 'On Going') {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => unitRetrieval(
                        brand: orders.brand,
                        type: orders.type,
                        pic: orders.pic,
                        endingDate: orders.endDate,
                        startingDate: orders.startDate,
                        total: orders.totalPrice,
                        price: orders.price)));
          } else {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Invoice(
                        brand: orders.brand,
                        type: orders.type,
                        totalPrice: orders.totalPrice,
                        startDate: orders.startDate,
                        endDate: orders.endDate,
                        pic: orders.pic,
                        userId: orders.userId)));
          }
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 109,
          decoration: BoxDecoration(
            color: const Color(0xffD9D9D9),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8)),
                child: Image.network(
                  orders.pic != ''
                      ? 'https://drive.google.com/uc?export=view&id=' +
                          orders.pic.toString()
                      : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Rent Date: ',
                    style: GoogleFonts.montserrat(
                        fontSize: 8, fontWeight: FontWeight.w700),
                  ),
                  Text(
                    rentDate(),
                    style: GoogleFonts.montserrat(fontSize: 8),
                  ),
                  Text(
                    orders.type.toString(),
                    style: GoogleFonts.montserrat(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Text(
                    orders.brand.toString(),
                    style: GoogleFonts.montserrat(
                      fontSize: 12,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        orders.status.toString(),
                        style: GoogleFonts.montserrat(
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
