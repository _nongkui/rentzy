// ignore_for_file: use_build_context_synchronously, dead_code, prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rentzy_rpl/auth/auth.dart';
import 'package:rentzy_rpl/auth/login.dart';

class Register extends StatefulWidget {
  final VoidCallback showLoginPage;
  const Register({
    super.key,
    required this.showLoginPage,
  });

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _repasswordController = TextEditingController();
  final _usernameController = TextEditingController();

  bool showPassword = true;
  bool showRePassword = true;
  String? passError;
  String? emailError;
  String? rePassError;

  checkPassword() {
    if (_passwordController.text == _repasswordController.text) {
      return _passwordController.text.trim();
    } else {
      setState(() {
        rePassError = "Password missmatch";
      });
    }
  }

  Future register() async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: _emailController.text.trim(),
        password: checkPassword(),
      );
      await FirebaseAuth.instance.currentUser!.updateDisplayName(
        _usernameController.text.trim(),
      );
      await FirebaseFirestore.instance
          .collection('users')
          .add({'role': 'customer'});
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const Auth(),
        ),
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        setState(() {
          passError = 'The password is too weak.';
        });
      } else if (e.code == 'email-already-in-use') {
        setState(() {
          emailError = 'The account already exist for that email.';
        });
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _repasswordController.dispose();
    _usernameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 24, left: 24, top: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // ignore: prefer_const_literals_to_create_immutables
                  children: [
                    Center(
                      child: SvgPicture.asset(
                        'assets/register.svg',
                        height: 260,
                        width: 258.28,
                      ),
                    ),
                    const SizedBox(
                      height: 29,
                    ),

                    //Register Title
                    Text(
                      "REGISTER",
                      style: GoogleFonts.montserrat(
                          fontSize: 36,
                          color: const Color(0xff0E0F0E),
                          fontWeight: FontWeight.w600,
                          letterSpacing: 1),
                    ),

                    const SizedBox(
                      height: 36,
                    ),

                    //Email input form
                    TextField(
                      controller: _emailController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.redAccent),
                            borderRadius: BorderRadius.circular(10)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.redAccent),
                            borderRadius: BorderRadius.circular(10)),
                        errorText: emailError,
                        border: InputBorder.none,
                        hintText: "Email",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 24,
                    ),

                    //Username input form
                    TextField(
                      controller: _usernameController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.redAccent),
                            borderRadius: BorderRadius.circular(10)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.redAccent),
                            borderRadius: BorderRadius.circular(10)),
                        border: InputBorder.none,
                        hintText: "Username",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 24,
                    ),

                    //password input form
                    TextField(
                      obscureText: showPassword,
                      controller: _passwordController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.redAccent),
                            borderRadius: BorderRadius.circular(10)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.redAccent),
                            borderRadius: BorderRadius.circular(10)),
                        errorText: passError,
                        border: InputBorder.none,
                        hintText: "Password",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                        suffixIcon: IconButton(
                          onPressed: (() {
                            setState(() {
                              showPassword = !showPassword;
                            });
                          }),
                          icon: Icon(
                            showPassword
                                ? FeatherIcons.eye
                                : FeatherIcons.eyeOff,
                            size: 20,
                            color: Color(0xff0E0F0E),
                          ),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 24,
                    ),

                    //Re password input form
                    TextField(
                      obscureText: showRePassword,
                      controller: _repasswordController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.redAccent),
                            borderRadius: BorderRadius.circular(10)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.redAccent),
                            borderRadius: BorderRadius.circular(10)),
                        errorText: rePassError,
                        border: InputBorder.none,
                        hintText: "Re - Password",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                        suffixIcon: IconButton(
                          onPressed: (() {
                            setState(() {
                              showRePassword = !showRePassword;
                            });
                          }),
                          icon: Icon(
                            showRePassword
                                ? FeatherIcons.eye
                                : FeatherIcons.eyeOff,
                            size: 20,
                            color: Color(0xff0E0F0E),
                          ),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 24,
                    ),

                    //register button
                    GestureDetector(
                      // onTap: (() {
                      //   Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => RegisterOtp()),
                      //   );
                      // }),
                      onTap: () {
                        register();
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 52,
                        decoration: BoxDecoration(
                          color: const Color(0xffF54768),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: Text(
                            'REGISTER',
                            style: GoogleFonts.montserrat(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1),
                          ),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 16,
                    ),

                    //already have account text button
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Already have an account?',
                          style: GoogleFonts.montserrat(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                              color: const Color(0xff0E0F0E)),
                        ),
                        GestureDetector(
                          onTap: widget.showLoginPage,
                          child: Text(
                            ' Login',
                            style: GoogleFonts.montserrat(
                                fontSize: 14,
                                color: const Color(0xff41436A),
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
