// ignore_for_file: prefer_const_constructors, prefer_final_fields

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:rentzy_rpl/pages/home.dart';
import 'package:rentzy_rpl/pages/landing.dart';

import '../pages/admin/adminHome.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  dynamic adminId;
  isAdmin() {
    FirebaseFirestore.instance
        .collection('users')
        .where('role', isEqualTo: 'admin')
        .get()
        .then((value) => value.docs.forEach((element) {
              FirebaseFirestore.instance.collection('users').doc(element.id);
              setState(() {
                adminId = element.id;
              });
            }));
    return adminId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (FirebaseAuth.instance.currentUser?.uid == isAdmin()) {
              return adminHome();
            } else {
              return Home();
            }
          } else {
            return Landing();
          }
        },
      ),
    );
  }
}
