// ignore_for_file: prefer_const_constructors, prefer_interpolation_to_compose_strings
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/models/orders.dart';
import 'package:rentzy_rpl/pages/home.dart';

typedef StringValue = String Function(String);

class paymentDetails extends StatefulWidget {
  final dynamic pic;
  final dynamic type;
  final dynamic brand;
  final dynamic price;
  final dynamic startDate;
  final dynamic endDate;
  final dynamic total;

  paymentDetails({
    super.key,
    required this.pic,
    required this.price,
    required this.type,
    required this.brand,
    required this.startDate,
    required this.endDate,
    required this.total,
  });

  @override
  State<paymentDetails> createState() => _paymentDetailsState();
}

class _paymentDetailsState extends State<paymentDetails> {
  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("Cash"), value: "Cash"),
      DropdownMenuItem(child: Text("Bank Transfer"), value: "Bank Transfer"),
    ];
    return menuItems;
  }

  Orders orders = Orders();

  addPayment() {
    orders.startDate = widget.startDate.toString();
    orders.endDate = widget.endDate.toString();
    orders.status = 'Waiting for Payment';
    orders.type = widget.type;
    orders.brand = widget.brand;
    orders.pic = widget.pic;
    orders.method = selectedValue;
    orders.price = widget.price;
    orders.totalPrice = widget.total;
    orders.confirmed = false;
    orders.userId = FirebaseAuth.instance.currentUser!.uid;

    FirebaseFirestore.instance.collection('orders').add(orders.toJson());
  }

  String selectedValue = "Cash";

  @override
  Widget build(BuildContext context) {
    var strStartMonth = widget.startDate.toString().substring(5, 7);
    var startDay = widget.startDate.toString().substring(8, 10);
    var startYear = widget.startDate.toString().substring(0, 4);
    var startMonth;
    int intStartMonth = int.parse(strStartMonth);
    var _startDate;
    switch (intStartMonth) {
      case 1:
        {
          setState(() {
            startMonth = 'January';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 2:
        {
          setState(() {
            startMonth = 'February';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 3:
        {
          setState(() {
            startMonth = 'March';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 4:
        {
          setState(() {
            startMonth = 'April';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 5:
        {
          setState(() {
            startMonth = 'May';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 6:
        {
          setState(() {
            startMonth = 'June';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 7:
        {
          setState(() {
            startMonth = 'July';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 8:
        {
          setState(() {
            startMonth = 'August';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 9:
        {
          setState(() {
            startMonth = 'September';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 10:
        {
          setState(() {
            startMonth = 'October';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 11:
        {
          setState(() {
            startMonth = 'November';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 12:
        {
          setState(() {
            startMonth = 'Desember';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      default:
    }

    var strEndMonth = widget.endDate.toString().substring(5, 7);
    var endDay = widget.endDate.toString().substring(8, 10);
    var endYear = widget.endDate.toString().substring(0, 4);
    var endMonth;
    int intEndMonth = int.parse(strStartMonth);
    var _endDate;
    switch (intEndMonth) {
      case 1:
        {
          setState(() {
            endMonth = 'January';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 2:
        {
          setState(() {
            endMonth = 'February';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 3:
        {
          setState(() {
            endMonth = 'March';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 4:
        {
          setState(() {
            endMonth = 'April';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 5:
        {
          setState(() {
            endMonth = 'May';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 6:
        {
          setState(() {
            endMonth = 'June';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 7:
        {
          setState(() {
            endMonth = 'July';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 8:
        {
          setState(() {
            endMonth = 'August';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 9:
        {
          setState(() {
            endMonth = 'September';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 10:
        {
          setState(() {
            endMonth = 'October';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 11:
        {
          setState(() {
            endMonth = 'November';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 12:
        {
          setState(() {
            endMonth = 'Desember';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      default:
    }
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 24,
            right: 24,
            top: 32,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      FeatherIcons.chevronLeft,
                      size: 32,
                      color: Color(0xff1E1E1E),
                    ),
                  ),
                  SizedBox(
                    width: 24,
                  ),
                  Text(
                    'Payment Details',
                    style: GoogleFonts.montserrat(
                      fontSize: 32,
                      fontWeight: FontWeight.w500,
                      color: Color(0xff1E1E1E),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 40,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Your Bike',
                    style: GoogleFonts.montserrat(
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                      color: Color(0xff1E1E1E),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Color(0xffD9D9D9),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                              bottomLeft: Radius.circular(8)),
                          child: Image.network(
                            widget.pic != ''
                                ? 'https://drive.google.com/uc?export=view&id=' +
                                    widget.pic
                                : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.type,
                              style: GoogleFonts.montserrat(
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                color: Color(0xff1E1E1E),
                              ),
                            ),
                            Text(
                              widget.brand,
                              style: GoogleFonts.montserrat(
                                fontSize: 12,
                                fontWeight: FontWeight.w300,
                                color: Color(0xff1E1E1E),
                              ),
                            ),
                            SizedBox(
                              height: 24,
                            ),
                            Row(
                              children: [
                                Text(
                                  'Rp ',
                                  style: GoogleFonts.montserrat(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xff1E1E1E),
                                  ),
                                ),
                                Text(
                                  widget.price.toString(),
                                  style: GoogleFonts.montserrat(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xff1E1E1E),
                                  ),
                                ),
                                Text(
                                  ' / Day',
                                  style: GoogleFonts.montserrat(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w300,
                                    color: Color(0xff1E1E1E),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    'Starting Date',
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff1E1E1E),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 56,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color(0xff41436A)),
                      borderRadius: BorderRadius.circular(8),
                      color: Color(0xffFFFFFF),
                    ),
                    child: Center(
                      child: Text(
                        _startDate,
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    'Ending Date',
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 56,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color(0xff41436A)),
                      borderRadius: BorderRadius.circular(8),
                      color: Color(0xffFFFFFF),
                    ),
                    child: Center(
                      child: Text(
                        _endDate,
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Total',
                        style: GoogleFonts.montserrat(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            'Rp',
                            style: GoogleFonts.montserrat(
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            widget.total.toString(),
                            style: GoogleFonts.montserrat(
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Text(
                    'Choose your payment method to pay the rental fee.',
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 56,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Color(0xff000000)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 8,
                        right: 8,
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          hint: Text(
                            'Select Payment Method!',
                            style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                                color: Color(
                                  0xff000000,
                                )),
                          ),
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(
                              0xff000000,
                            ),
                          ),
                          value: selectedValue,
                          items: dropdownItems,
                          onChanged: (String? newValue) {
                            setState(() {
                              selectedValue = newValue!;
                            });
                          },
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 52,
                  ),
                  Center(
                    child: GestureDetector(
                      onTap: () {
                        addPayment();
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) => Home()));
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 56,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Color(0xff41436A),
                        ),
                        child: Center(
                          child: Text(
                            'Confirm',
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
