// ignore_for_file: prefer_const_constructors, prefer_interpolation_to_compose_strings

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/pages/rent.dart';

class Units extends StatelessWidget {
  final DocumentSnapshot details;
  Units({required this.details});

  @override
  Widget build(BuildContext context) {
    var available = details['available'];
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.only(
          right: 24,
          left: 24,
          top: 32,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    FeatherIcons.chevronLeft,
                    size: 32,
                    color: Color(0xff1E1E1E),
                  ),
                ),
                Text(
                  'Unit Details',
                  style: GoogleFonts.montserrat(
                    fontSize: 32,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff1E1E1E),
                  ),
                ),
                SizedBox(
                  width: 32,
                )
              ],
            ),

            SizedBox(
              height: 48,
            ),

            Container(
              decoration: BoxDecoration(
                color: Color(0xffD9D9D9),
                borderRadius: BorderRadius.circular(8),
              ),
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Image.network(
                      details['pic'] != ''
                          ? 'https://drive.google.com/uc?export=view&id=' +
                              details["pic"]
                          : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                      scale: 0.3,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 4,
                      bottom: 8,
                      top: 8,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          details['type'],
                          style: GoogleFonts.montserrat(
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(
                          details['brand'],
                          style: GoogleFonts.montserrat(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: [
                            Text(
                              'Rp ',
                              style: GoogleFonts.montserrat(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              details['price'].toString(),
                              style: GoogleFonts.montserrat(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              ' / Day',
                              style: GoogleFonts.montserrat(
                                fontSize: 20,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(
              height: 24,
            ),

            Text(
              "Specifications",
              style: GoogleFonts.montserrat(
                fontSize: 20,
                fontWeight: FontWeight.w500,
              ),
            ),

            SizedBox(
              height: 16,
            ),

            //Unit Specifications
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 128,
                  height: 57,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff0E0F0E),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          FeatherIcons.settings,
                          size: 22,
                        ),
                        Text(
                          details['gear'],
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: 128,
                  height: 57,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff0E0F0E),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          FeatherIcons.calendar,
                          size: 22,
                        ),
                        Text(
                          details['released'],
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),

            SizedBox(
              height: 16,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 128,
                  height: 57,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff0E0F0E),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          FeatherIcons.droplet,
                          size: 22,
                        ),
                        Text(
                          details['displacement'] + ' CC',
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: 128,
                  height: 57,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff0E0F0E),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          FeatherIcons.star,
                          size: 22,
                        ),
                        Text(
                          details['rating'],
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),

            SizedBox(
              height: 32,
            ),

            //Rent Button
            GestureDetector(
              onTap: available
                  ? () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Rent(
                                  price: details['price'],
                                  pic: details['pic'],
                                  type: details['type'],
                                  brand: details['brand'])));
                    }
                  : () {
                      showDialog(
                          context: context,
                          builder: ((context) => AlertDialog(
                                icon: Icon(
                                  FeatherIcons.info,
                                  color: Colors.redAccent,
                                ),
                                title: Text(
                                  'Unit Info',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 24, color: Color(0xff0E0F0E)),
                                ),
                                content: Text(
                                  'This unit is currently unavailable',
                                  style: GoogleFonts.montserrat(
                                    fontSize: 12,
                                    color: Color(0xff0E0F0E),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                              )));
                    },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 52,
                decoration: BoxDecoration(
                  color: available ? Color(0xff41436A) : Color(0xffd9d9d9),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Rent Now",
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                    Icon(
                      FeatherIcons.chevronRight,
                      color: Colors.white,
                      size: 24,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
