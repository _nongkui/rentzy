// ignore_for_file: prefer_const_constructors, prefer_interpolation_to_compose_strings, use_build_context_synchronously

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/pages/admin/unitEdit.dart';

class unitDetails extends StatelessWidget {
  final DocumentSnapshot details;
  const unitDetails({super.key, required this.details});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        forceMaterialTransparency: true,
        centerTitle: true,
        title: Text(
          'Unit Details',
          style: GoogleFonts.montserrat(
              fontSize: 32,
              fontWeight: FontWeight.w500,
              color: Color(0xff0E0F0E)),
        ),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            FeatherIcons.chevronLeft,
            color: Color(0xff0E0F0E),
            size: 32,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.only(
          top: 32,
          left: 24,
          right: 24,
        ),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Color(0xffD9D9D9),
                borderRadius: BorderRadius.circular(8),
              ),
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Image.network(
                      details['pic'] == ''
                          ? 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni'
                          : 'https://drive.google.com/uc?export=view&id=' +
                              details['pic'],
                      scale: 0.3,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 4,
                      bottom: 8,
                      top: 8,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          details['type'],
                          style: GoogleFonts.montserrat(
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(
                          details['brand'],
                          style: GoogleFonts.montserrat(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: [
                            Text(
                              'Rp ',
                              style: GoogleFonts.montserrat(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              details['price'].toString(),
                              style: GoogleFonts.montserrat(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              ' / Day',
                              style: GoogleFonts.montserrat(
                                fontSize: 20,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 16,
            ),

            Text(
              "Specifications",
              style: GoogleFonts.montserrat(
                fontSize: 20,
                fontWeight: FontWeight.w500,
              ),
            ),

            SizedBox(
              height: 16,
            ),

            //Unit Specifications
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    width: 128,
                    height: 57,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xff0E0F0E),
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            FeatherIcons.settings,
                            size: 22,
                          ),
                          Text(
                            details['gear'],
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    width: 128,
                    height: 57,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xff0E0F0E),
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            FeatherIcons.calendar,
                            size: 22,
                          ),
                          Text(
                            details['released'],
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),

            SizedBox(
              height: 8,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    width: 128,
                    height: 57,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xff0E0F0E),
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            FeatherIcons.droplet,
                            size: 22,
                          ),
                          Text(
                            details['displacement'] + ' CC',
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    width: 128,
                    height: 57,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xff0E0F0E),
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            FeatherIcons.star,
                            size: 22,
                          ),
                          Text(
                            details['rating'],
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 16,
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 52),
                backgroundColor: Color(0xff41436A),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => unitEdit(
                              type: details['type'],
                              brand: details['brand'],
                              price: details['price'],
                              pic: details['pic'],
                              availability: details['available'],
                              displacement: details['displacement'],
                              gear: details['gear'],
                              released: details['released'],
                              rating: details['rating'],
                            )));
              },
              label: Text(
                'Edit Info',
                style: GoogleFonts.montserrat(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
              icon: Icon(
                FeatherIcons.edit,
                size: 18,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 52),
                backgroundColor: Colors.redAccent,
              ),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: ((context) => AlertDialog(
                          actions: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text(
                                        'Cancel',
                                        style: GoogleFonts.montserrat(
                                          fontSize: 12,
                                          color: Colors.black87,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      )),
                                ),
                                Expanded(
                                  child: TextButton(
                                      onPressed: () {
                                        FirebaseFirestore.instance
                                            .collection('units')
                                            .doc(details.id)
                                            .delete();
                                        Navigator.pop(context);
                                      },
                                      child: Text(
                                        'Delete',
                                        style: GoogleFonts.montserrat(
                                          fontSize: 12,
                                          color: Colors.redAccent,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      )),
                                )
                              ],
                            ),
                          ],
                          icon: Icon(
                            FeatherIcons.info,
                            color: Colors.redAccent,
                          ),
                          title: Text(
                            'Unit Deletion',
                            style: GoogleFonts.montserrat(
                                fontSize: 24, color: Color(0xff0E0F0E)),
                          ),
                          content: Text(
                            'Are you sure?',
                            style: GoogleFonts.montserrat(
                              fontSize: 12,
                              color: Color(0xff0E0F0E),
                            ),
                            textAlign: TextAlign.center,
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                        )));
                Navigator.pop(context);
              },
              label: Text(
                'Delete Unit',
                style: GoogleFonts.montserrat(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
              icon: Icon(
                FeatherIcons.trash,
                size: 18,
              ),
            )
          ],
        ),
      )),
    );
  }
}
