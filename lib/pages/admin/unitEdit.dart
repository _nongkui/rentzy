// ignore_for_file: prefer_const_constructors, prefer_interpolation_to_compose_strings, sort_child_properties_last, use_build_context_synchronously

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class unitEdit extends StatefulWidget {
  dynamic type;
  dynamic brand;
  int price;
  dynamic availability;
  dynamic gear;
  dynamic displacement;
  dynamic rating;
  dynamic released;
  dynamic pic;

  unitEdit(
      {super.key,
      required this.type,
      required this.brand,
      required this.price,
      required this.availability,
      required this.gear,
      required this.displacement,
      required this.rating,
      required this.released,
      required this.pic});

  @override
  State<unitEdit> createState() => _unitEditState();
}

class _unitEditState extends State<unitEdit> {
  bool? available = true;

  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("Type"), value: "type"),
      DropdownMenuItem(child: Text("Brand"), value: "brand"),
      DropdownMenuItem(child: Text("Price"), value: "price"),
      DropdownMenuItem(child: Text("Gear"), value: "gear"),
      DropdownMenuItem(child: Text("Released"), value: "released"),
      DropdownMenuItem(child: Text("Displacement"), value: "displacement"),
      DropdownMenuItem(child: Text("Rating"), value: "rating"),
      DropdownMenuItem(child: Text("Pic"), value: "pic"),
    ];
    return menuItems;
  }

  String selectedValue = "type";

  final _updateField = TextEditingController();

  @override
  Widget build(BuildContext context) {
    dynamic editField;
    switch (selectedValue) {
      case 'type':
        setState(() {
          editField = 'type';
        });
        break;
      case 'brand':
        setState(() {
          editField = 'brand';
        });
        break;
      case 'price':
        setState(() {
          editField = 'price';
        });
        break;
      case 'gear':
        setState(() {
          editField = 'gear';
        });
        break;
      case 'released':
        setState(() {
          editField = 'released';
        });
        break;
      case 'pic':
        setState(() {
          editField = 'pic';
        });
        break;
      case 'rating':
        setState(() {
          editField = 'rating';
        });
        break;
      case 'displacement':
        setState(() {
          editField = 'displacement';
        });
        break;
      default:
    }
    return Scaffold(
      appBar: AppBar(
        forceMaterialTransparency: true,
        centerTitle: true,
        title: Text(
          'Edit Unit Details',
          style: GoogleFonts.montserrat(
              fontSize: 32,
              fontWeight: FontWeight.w500,
              color: Color(0xff0E0F0E)),
        ),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            FeatherIcons.chevronLeft,
            color: Color(0xff0E0F0E),
            size: 32,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.only(
          top: 32,
          left: 24,
          right: 24,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Color(0xffD9D9D9),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      child: Image.network(
                        widget.pic == ''
                            ? 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni'
                            : 'https://drive.google.com/uc?export=view&id=' +
                                widget.pic,
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 4,
                        bottom: 8,
                        top: 8,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.type,
                            style: GoogleFonts.montserrat(
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            widget.brand,
                            style: GoogleFonts.montserrat(
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Row(
                            children: [
                              Text(
                                'Rp ',
                                style: GoogleFonts.montserrat(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Text(
                                widget.price.toString(),
                                style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Text(
                                ' / Day',
                                style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),

              Text(
                "Specifications",
                style: GoogleFonts.montserrat(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Color(0xff0E0F0E),
                ),
              ),

              SizedBox(
                height: 16,
              ),

              //Unit Specifications
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      width: 128,
                      height: 57,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xff0E0F0E),
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Icon(
                              FeatherIcons.settings,
                              size: 22,
                            ),
                            Text(
                              widget.gear,
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      width: 128,
                      height: 57,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xff0E0F0E),
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Icon(
                              FeatherIcons.calendar,
                              size: 22,
                            ),
                            Text(
                              widget.released,
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),

              SizedBox(
                height: 8,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      width: 128,
                      height: 57,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xff0E0F0E),
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Icon(
                              FeatherIcons.droplet,
                              size: 22,
                            ),
                            Text(
                              widget.displacement + ' CC',
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      width: 128,
                      height: 57,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xff0E0F0E),
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Icon(
                              FeatherIcons.star,
                              size: 22,
                            ),
                            Text(
                              widget.rating,
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                'Select field to update',
                style: GoogleFonts.montserrat(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Color(0xff0E0F0E),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 56,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Color(0xff000000)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 8,
                    right: 8,
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      hint: Text(
                        'Select Update Field!',
                        style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                            color: Color(
                              0xff000000,
                            )),
                      ),
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(
                          0xff000000,
                        ),
                      ),
                      value: selectedValue,
                      items: dropdownItems,
                      onChanged: (String? newValue) {
                        setState(() {
                          selectedValue = newValue!;
                        });
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              TextField(
                controller: _updateField,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xffd9d9d9)),
                      borderRadius: BorderRadius.circular(10)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff41436A)),
                      borderRadius: BorderRadius.circular(10)),
                  disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Color(0xffd9d9d9))),
                  border: InputBorder.none,
                  hintText: editField,
                  hintStyle: GoogleFonts.montserrat(
                    fontSize: 16,
                    fontWeight: FontWeight.w300,
                    color: const Color(0xff0E0F0E),
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Column(
                children: [
                  RadioListTile(
                      activeColor: Color(0xff41436A),
                      title: Text(
                        'Available',
                        style: GoogleFonts.montserrat(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                      value: true,
                      groupValue: available,
                      onChanged: (value) {
                        setState(() {
                          available = true;
                        });
                      }),
                  RadioListTile(
                      activeColor: Color(0xff41436A),
                      toggleable: true,
                      title: Text(
                        'Unavailable',
                        style: GoogleFonts.montserrat(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                      value: false,
                      groupValue: available,
                      onChanged: (value) {
                        setState(() {
                          available = false;
                        });
                      }),
                ],
              ),
              SizedBox(
                height: 24,
              ),
              ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                  fixedSize: Size(MediaQuery.of(context).size.width, 52),
                  backgroundColor: Color(0xff41436A),
                ),
                onPressed: () async {
                  await FirebaseFirestore.instance
                      .collection('units')
                      .where('type', isEqualTo: widget.type)
                      .get()
                      .then(
                        (value) => value.docs.forEach((element) {
                          if (editField == 'price') {
                            FirebaseFirestore.instance
                                .collection('units')
                                .doc(element.id)
                                .update({
                              editField: int.parse(_updateField.text.trim())
                            });
                          } else {
                            FirebaseFirestore.instance
                                .collection('units')
                                .doc(element.id)
                                .update({editField: _updateField.text.trim()});
                          }

                          if (available == false) {
                            FirebaseFirestore.instance
                                .collection('units')
                                .doc(element.id)
                                .update({'available': available});
                          } else {
                            FirebaseFirestore.instance
                                .collection('units')
                                .doc(element.id)
                                .update({'available': true});
                          }
                        }),
                      );

                  await showDialog(
                      context: context,
                      builder: ((context) => AlertDialog(
                            icon: Icon(
                              FeatherIcons.info,
                              color: Colors.greenAccent,
                            ),
                            title: Text(
                              'Unit Updated',
                              style: GoogleFonts.montserrat(
                                  fontSize: 24, color: Color(0xff0E0F0E)),
                            ),
                            content: Text(
                              'The unit info has been updated!',
                              style: GoogleFonts.montserrat(
                                fontSize: 12,
                                color: Color(0xff0E0F0E),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                          )));
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                label: Text(
                  'Edit Unit',
                  style: GoogleFonts.montserrat(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                icon: Icon(
                  FeatherIcons.edit,
                  size: 18,
                ),
              )
            ],
          ),
        ),
      )),
    );
  }
}
