// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace, unused_field, use_build_context_synchronously

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/models/unitsModels.dart';

class manageUnits extends StatefulWidget {
  const manageUnits({super.key});

  @override
  State<manageUnits> createState() => _manageUnitsState();
}

class _manageUnitsState extends State<manageUnits> {
  bool _addUnit = false;
  final _typeController = TextEditingController();
  final _brandController = TextEditingController();
  final _priceController = TextEditingController();
  final _displacementController = TextEditingController();
  final _gearController = TextEditingController();
  final _ratingController = TextEditingController();
  final _releasedController = TextEditingController();
  final _picController = TextEditingController();

  UnitsModels units = UnitsModels();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        forceMaterialTransparency: true,
        centerTitle: true,
        title: Text(
          'Manage Units',
          style: GoogleFonts.montserrat(
              fontSize: 32,
              fontWeight: FontWeight.w500,
              color: Color(0xff0E0F0E)),
        ),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            FeatherIcons.chevronLeft,
            color: Color(0xff0E0F0E),
            size: 32,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
              left: 24,
              right: 24,
            ),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 48),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Color(0xffD9D9D9),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8),
                            bottomLeft: Radius.circular(8)),
                        child: Image.network(
                          'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                          scale: 1.1,
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            _typeController.text != ''
                                ? _typeController.text.trim()
                                : 'Type',
                            style: GoogleFonts.montserrat(
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            _brandController.text != ''
                                ? _brandController.text.trim()
                                : 'Brand',
                            style: GoogleFonts.montserrat(
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            children: [
                              Text(
                                'Rp ',
                                style: GoogleFonts.montserrat(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Text(
                                _priceController.text != ''
                                    ? _priceController.text.trim()
                                    : 'Price',
                                style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Text(
                                ' / Day',
                                style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                ElevatedButton.icon(
                  icon: Icon(
                    FeatherIcons.plus,
                    size: 16,
                  ),
                  style: ElevatedButton.styleFrom(
                    fixedSize: Size(MediaQuery.of(context).size.width, 52),
                    backgroundColor: Color(0xff974063),
                  ),
                  onPressed: () {
                    setState(() {
                      _addUnit = !_addUnit;
                    });
                  },
                  label: Text(
                    'Add a Unit',
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 360,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  child: ListView(children: [
                    TextField(
                      controller: _typeController,
                      enabled: _addUnit,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _addUnit ? Colors.white : Color(0xffd9d9d9),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xffd9d9d9))),
                        border: InputBorder.none,
                        hintText: "Type",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TextField(
                      controller: _brandController,
                      enabled: _addUnit,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _addUnit ? Colors.white : Color(0xffd9d9d9),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xffd9d9d9))),
                        border: InputBorder.none,
                        hintText: "Brand",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TextField(
                      controller: _priceController,
                      enabled: _addUnit,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _addUnit ? Colors.white : Color(0xffd9d9d9),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xffd9d9d9))),
                        border: InputBorder.none,
                        hintText: "Price",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TextField(
                      controller: _displacementController,
                      enabled: _addUnit,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _addUnit ? Colors.white : Color(0xffd9d9d9),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xffd9d9d9))),
                        border: InputBorder.none,
                        hintText: "Displacement",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TextField(
                      controller: _gearController,
                      enabled: _addUnit,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _addUnit ? Colors.white : Color(0xffd9d9d9),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xffd9d9d9))),
                        border: InputBorder.none,
                        hintText: "Gear",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TextField(
                      controller: _ratingController,
                      enabled: _addUnit,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _addUnit ? Colors.white : Color(0xffd9d9d9),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xffd9d9d9))),
                        border: InputBorder.none,
                        hintText: "Rating",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TextField(
                      controller: _releasedController,
                      enabled: _addUnit,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _addUnit ? Colors.white : Color(0xffd9d9d9),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xffd9d9d9))),
                        border: InputBorder.none,
                        hintText: "Released",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TextField(
                      controller: _picController,
                      enabled: _addUnit,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _addUnit ? Colors.white : Color(0xffd9d9d9),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffd9d9d9)),
                            borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(10)),
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xffd9d9d9))),
                        border: InputBorder.none,
                        hintText: "Pic",
                        hintStyle: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: const Color(0xff0E0F0E),
                        ),
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: 24,
                ),
                ElevatedButton.icon(
                  onPressed: () async {
                    await saveUnit();
                    await showDialog(
                        context: context,
                        builder: ((context) => AlertDialog(
                              icon: Icon(
                                FeatherIcons.info,
                                color: Colors.greenAccent,
                              ),
                              title: Text(
                                'Unit Added',
                                style: GoogleFonts.montserrat(
                                    fontSize: 24, color: Color(0xff0E0F0E)),
                              ),
                              content: Text(
                                'New unit added to the list!',
                                style: GoogleFonts.montserrat(
                                  fontSize: 12,
                                  color: Color(0xff0E0F0E),
                                ),
                                textAlign: TextAlign.center,
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                            )));
                    clearText();
                  },
                  icon: Icon(
                    FeatherIcons.save,
                    size: 16,
                  ),
                  label: Text(
                    'Save Unit',
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(MediaQuery.of(context).size.width, 52),
                      backgroundColor: Color(0xff974063)),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  saveUnit() async {
    units.type = _typeController.text.trim();
    units.brand = _brandController.text.trim();
    units.price = int.parse(_priceController.text.trim());
    units.available = true;
    units.displacement = _displacementController.text.trim();
    units.gear = _gearController.text.trim();
    units.rating = _ratingController.text.trim();
    units.released = _releasedController.text.trim();
    units.pic = _picController.text.trim();

    await FirebaseFirestore.instance.collection('units').add(units.toJson());
  }

  void clearText() {
    _typeController.clear();
    _brandController.clear();
    _priceController.clear();
    _displacementController.clear();
    _gearController.clear();
    _ratingController.clear();
    _releasedController.clear();
    _picController.clear();
  }
}
