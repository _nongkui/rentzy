// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace, prefer_interpolation_to_compose_strings

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/pages/admin/orderConfirm.dart';
import 'package:rentzy_rpl/pages/admin/pickUpConfirm.dart';

class upComingOrders extends StatefulWidget {
  const upComingOrders({super.key});

  @override
  State<upComingOrders> createState() => _upComingOrdersState();
}

class _upComingOrdersState extends State<upComingOrders> {
  bool activeUnconfirmed = false;
  bool activeWaiting = false;
  dynamic db;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        forceMaterialTransparency: true,
        centerTitle: true,
        title: Text(
          'Incoming Orders',
          style: GoogleFonts.montserrat(
              fontSize: 32,
              fontWeight: FontWeight.w500,
              color: Color(0xff0E0F0E)),
        ),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            FeatherIcons.chevronLeft,
            color: Color(0xff0E0F0E),
            size: 32,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.only(
          top: 32,
          left: 24,
          right: 24,
        ),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  height: 48,
                  decoration: BoxDecoration(
                      color: Color(0xffc4c4c4),
                      borderRadius: BorderRadius.circular(20)),
                  width: MediaQuery.of(context).size.width - 48,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                activeWaiting = false;
                                activeUnconfirmed = true;
                                db = FirebaseFirestore.instance
                                    .collection('orders')
                                    .where('confirmed', isEqualTo: false)
                                    .snapshots();
                              });
                            },
                            child: Container(
                                height: 32,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50),
                                    color: activeUnconfirmed
                                        ? Color(0xff41436A)
                                        : Color(0xffc4c4c4)),
                                child: Center(
                                    child: Text(
                                  'Unconfirmed',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      color: activeUnconfirmed
                                          ? Colors.white
                                          : Color(0xff0E0F0E)),
                                ))),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  activeWaiting = true;
                                  activeUnconfirmed = false;
                                  db = FirebaseFirestore.instance
                                      .collection('orders')
                                      .where('status',
                                          isEqualTo: 'Waiting for Pick Up')
                                      .snapshots();
                                });
                              },
                              child: Container(
                                  height: 32,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: activeWaiting
                                          ? Color(0xff41436A)
                                          : Color(0xffc4c4c4)),
                                  child: Center(
                                      child: Text(
                                    'Waiting for Pick Up',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        color: activeWaiting
                                            ? Colors.white
                                            : Color(0xff0E0F0E)),
                                  ))),
                            )),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.7,
                child: StreamBuilder<QuerySnapshot>(
                  stream: db,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return ListView(
                      children: snapshot.data!.docs
                          .map((doc) => Padding(
                                padding: const EdgeInsets.only(bottom: 8),
                                child: GestureDetector(
                                  onTap: () {
                                    if (activeUnconfirmed == true) {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  orderConfirm(details: doc)));
                                    } else {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  pickUpConfirm(details: doc)));
                                    }
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Color(0xffD9D9D9),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Row(
                                      children: [
                                        ClipRRect(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(8),
                                              bottomLeft: Radius.circular(8)),
                                          child: Image.network(
                                            doc['pic'] != ''
                                                ? 'https://drive.google.com/uc?export=view&id=' +
                                                    doc["pic"]
                                                : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                                          ),
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              doc['type'],
                                              style: GoogleFonts.montserrat(
                                                fontSize: 24,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                            Text(
                                              doc['brand'],
                                              style: GoogleFonts.montserrat(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w300,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 24,
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  'Rp ',
                                                  style: GoogleFonts.montserrat(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                                Text(
                                                  doc['price'].toString(),
                                                  style: GoogleFonts.montserrat(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                                Text(
                                                  ' / Day',
                                                  style: GoogleFonts.montserrat(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.w300,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ))
                          .toList(),
                    );
                  },
                ))
          ],
        ),
      )),
    );
  }
}
