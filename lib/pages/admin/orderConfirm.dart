// ignore_for_file: prefer_const_constructors, use_build_context_synchronously, prefer_interpolation_to_compose_strings

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class orderConfirm extends StatefulWidget {
  final DocumentSnapshot details;
  const orderConfirm({super.key, required this.details});

  @override
  State<orderConfirm> createState() => _orderConfirmState();
}

class _orderConfirmState extends State<orderConfirm> {
  @override
  Widget build(BuildContext context) {
    var strStartMonth = widget.details['startDate'].toString().substring(5, 7);
    var startDay = widget.details['startDate'].toString().substring(8, 10);
    var startYear = widget.details['startDate'].toString().substring(0, 4);
    var startMonth;
    int intStartMonth = int.parse(strStartMonth);
    var _startDate;
    switch (intStartMonth) {
      case 1:
        {
          setState(() {
            startMonth = 'January';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 2:
        {
          setState(() {
            startMonth = 'February';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 3:
        {
          setState(() {
            startMonth = 'March';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 4:
        {
          setState(() {
            startMonth = 'April';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 5:
        {
          setState(() {
            startMonth = 'May';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 6:
        {
          setState(() {
            startMonth = 'June';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 7:
        {
          setState(() {
            startMonth = 'July';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 8:
        {
          setState(() {
            startMonth = 'August';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 9:
        {
          setState(() {
            startMonth = 'September';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 10:
        {
          setState(() {
            startMonth = 'October';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 11:
        {
          setState(() {
            startMonth = 'November';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 12:
        {
          setState(() {
            startMonth = 'Desember';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      default:
    }

    var strEndMonth = widget.details['endDate'].toString().substring(5, 7);
    var endDay = widget.details['endDate'].toString().substring(8, 10);
    var endYear = widget.details['endDate'].toString().substring(0, 4);
    var endMonth;
    int intEndMonth = int.parse(strStartMonth);
    var _endDate;
    switch (intEndMonth) {
      case 1:
        {
          setState(() {
            endMonth = 'January';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 2:
        {
          setState(() {
            endMonth = 'February';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 3:
        {
          setState(() {
            endMonth = 'March';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 4:
        {
          setState(() {
            endMonth = 'April';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 5:
        {
          setState(() {
            endMonth = 'May';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 6:
        {
          setState(() {
            endMonth = 'June';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 7:
        {
          setState(() {
            endMonth = 'July';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 8:
        {
          setState(() {
            endMonth = 'August';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 9:
        {
          setState(() {
            endMonth = 'September';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 10:
        {
          setState(() {
            endMonth = 'October';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 11:
        {
          setState(() {
            endMonth = 'November';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 12:
        {
          setState(() {
            endMonth = 'Desember';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      default:
    }
    return Scaffold(
      appBar: AppBar(
        forceMaterialTransparency: true,
        centerTitle: true,
        title: Text(
          'Upcoming Orders',
          style: GoogleFonts.montserrat(
              fontSize: 32,
              fontWeight: FontWeight.w500,
              color: Color(0xff0E0F0E)),
        ),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            FeatherIcons.chevronLeft,
            color: Color(0xff0E0F0E),
            size: 32,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.only(
          top: 32,
          left: 24,
          right: 24,
        ),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Color(0xffD9D9D9),
                borderRadius: BorderRadius.circular(8),
              ),
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        bottomLeft: Radius.circular(8)),
                    child: Image.network(
                      widget.details['pic'] != ''
                          ? 'https://drive.google.com/uc?export=view&id=' +
                              widget.details['pic']
                          : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.details['type'],
                        style: GoogleFonts.montserrat(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                          color: Color(0xff1E1E1E),
                        ),
                      ),
                      Text(
                        widget.details['brand'],
                        style: GoogleFonts.montserrat(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                          color: Color(0xff1E1E1E),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: [
                          Text(
                            'Rp ',
                            style: GoogleFonts.montserrat(
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                              color: Color(0xff1E1E1E),
                            ),
                          ),
                          Text(
                            widget.details['price'].toString(),
                            style: GoogleFonts.montserrat(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Color(0xff1E1E1E),
                            ),
                          ),
                          Text(
                            ' / Day',
                            style: GoogleFonts.montserrat(
                              fontSize: 20,
                              fontWeight: FontWeight.w300,
                              color: Color(0xff1E1E1E),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              'Starting Date',
              style: GoogleFonts.montserrat(
                fontSize: 16,
                fontWeight: FontWeight.w300,
                color: Color(0xff1E1E1E),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff41436A)),
                borderRadius: BorderRadius.circular(8),
                color: Color(0xffFFFFFF),
              ),
              child: Center(
                child: Text(
                  _startDate,
                  style: GoogleFonts.montserrat(
                    fontSize: 16,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              'Ending Date',
              style: GoogleFonts.montserrat(
                fontSize: 16,
                fontWeight: FontWeight.w300,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff41436A)),
                borderRadius: BorderRadius.circular(8),
                color: Color(0xffFFFFFF),
              ),
              child: Center(
                child: Text(
                  _endDate,
                  style: GoogleFonts.montserrat(
                    fontSize: 16,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 24,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total',
                  style: GoogleFonts.montserrat(
                    fontSize: 24,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Row(
                  children: [
                    Text(
                      'Rp',
                      style: GoogleFonts.montserrat(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(
                      widget.details['totalPrice'].toString(),
                      style: GoogleFonts.montserrat(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                  fixedSize: Size(
                    MediaQuery.of(context).size.width,
                    52,
                  ),
                  backgroundColor: Color(0xff41436A),
                ),
                onPressed: () async {
                  await FirebaseFirestore.instance
                      .collection('orders')
                      .doc(widget.details.id)
                      .update({'confirmed': true});
                  await showDialog(
                      context: context,
                      builder: ((context) => AlertDialog(
                            icon: Icon(
                              FeatherIcons.info,
                              color: Colors.greenAccent,
                            ),
                            title: Text(
                              'Order Confirmed',
                              style: GoogleFonts.montserrat(
                                  fontSize: 24, color: Color(0xff0E0F0E)),
                            ),
                            content: Text(
                              'New order added!',
                              style: GoogleFonts.montserrat(
                                fontSize: 12,
                                color: Color(0xff0E0F0E),
                              ),
                              textAlign: TextAlign.center,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                          )));
                  Navigator.pop(context);
                },
                child: Text(
                  'Confirm Order',
                  style: GoogleFonts.montserrat(
                      fontSize: 16, fontWeight: FontWeight.w500),
                ))
          ],
        ),
      )),
    );
  }
}
