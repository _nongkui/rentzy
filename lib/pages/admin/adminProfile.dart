// ignore_for_file: prefer_const_constructors

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../auth/main_page.dart';

class adminProfile extends StatelessWidget {
  const adminProfile({super.key});

  @override
  Widget build(BuildContext context) {
    dynamic user = FirebaseAuth.instance.currentUser!;
    return Scaffold(
      appBar: AppBar(
        forceMaterialTransparency: true,
        centerTitle: true,
        title: Text(
          'Profile',
          style: GoogleFonts.montserrat(
              fontSize: 32,
              fontWeight: FontWeight.w500,
              color: Color(0xff0E0F0E)),
        ),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            FeatherIcons.chevronLeft,
            color: Color(0xff0E0F0E),
            size: 32,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 32,
            right: 24,
            left: 24,
          ),
          child: Column(
            children: [
              Center(
                child: Column(
                  // ignore: prefer_const_literals_to_create_immutables
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage('assets/avatar-1.jpg'),
                      radius: 64,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      user.displayName!,
                      style: GoogleFonts.montserrat(
                        fontSize: 32,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff0E0F0E),
                      ),
                    ),
                    Text(
                      user.email!,
                      style: GoogleFonts.montserrat(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                        color: Color(0xff0E0F0E),
                      ),
                    ),
                    SizedBox(
                      height: 80,
                    ),

                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 52,
                      decoration: BoxDecoration(
                        color: Color(0xffffffff),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              FeatherIcons.user,
                              size: 20,
                              color: Color(0xff0E0F0E),
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            Text(
                              'Edit Profile',
                              style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff0E0F0E)),
                            ),
                            Spacer(
                              flex: 6,
                            ),
                            Icon(
                              FeatherIcons.chevronRight,
                              size: 20,
                            )
                          ],
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 24,
                    ),

                    //LogOut
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                                  icon: Icon(
                                    FeatherIcons.info,
                                    color: Colors.redAccent,
                                  ),
                                  title: Text(
                                    'Are you sure?',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 24, color: Color(0xff0E0F0E)),
                                  ),
                                  content: Text(
                                    'You will need to login again',
                                    style: GoogleFonts.montserrat(
                                      fontSize: 12,
                                      color: Color(0xff0E0F0E),
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  actions: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: Text(
                                                'Cancel',
                                                style: GoogleFonts.montserrat(
                                                  fontSize: 12,
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              )),
                                        ),
                                        Expanded(
                                          child: TextButton(
                                              onPressed: () {
                                                FirebaseAuth.instance.signOut();
                                                Navigator.pushReplacement(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        MainPage(),
                                                  ),
                                                );
                                              },
                                              child: Text(
                                                'Log Out',
                                                style: GoogleFonts.montserrat(
                                                  fontSize: 12,
                                                  color: Colors.redAccent,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              )),
                                        )
                                      ],
                                    ),
                                  ],
                                ));
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 52,
                        decoration: BoxDecoration(
                          color: Color(0xffF54768),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Icon(
                                FeatherIcons.logOut,
                                size: 20,
                                color: Color(0xffffffff),
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                'Log Out',
                                style: GoogleFonts.montserrat(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xffffffff)),
                              ),
                              Spacer(
                                flex: 6,
                              ),
                              Icon(
                                FeatherIcons.chevronRight,
                                size: 20,
                                color: Color(0xffffffff),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
