// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/pages/orderDetails.dart';
import 'package:table_calendar/table_calendar.dart';

class Rent extends StatefulWidget {
  final dynamic price;
  final dynamic pic;
  final dynamic type;
  final dynamic brand;
  Rent(
      {super.key,
      required this.price,
      required this.pic,
      required this.type,
      required this.brand});

  @override
  State<Rent> createState() => _RentState();
}

class _RentState extends State<Rent> {
  var _startDate;
  var _endDate;
  var total = 0;
  var stringStart;
  var stringEnd;
  var intStart;
  var intEnd;
  var totalPrice;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 32,
            right: 24,
            left: 24,
          ),
          child: Column(
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: const Icon(
                      FeatherIcons.chevronLeft,
                      size: 32,
                    ),
                  ),
                  const SizedBox(
                    width: 56,
                  ),
                  Text(
                    'Pick a Date!',
                    style: GoogleFonts.montserrat(
                      fontSize: 32,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              TableCalendar(
                focusedDay: DateTime.now(),
                firstDay: DateTime(2022),
                lastDay: DateTime(2025),
                rangeStartDay: _startDate,
                rangeEndDay: _endDate,
                calendarStyle: const CalendarStyle(
                  rangeHighlightColor: Color(0xffD9D9D9),
                  rangeStartDecoration: BoxDecoration(
                    color: Color(0xff41436A),
                    shape: BoxShape.circle,
                  ),
                  rangeEndDecoration: BoxDecoration(
                    color: Color(0xff41436A),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: (() async {
                      DateTime? startDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now(),
                        lastDate: DateTime(2025, 12, 10),
                      );

                      if (startDate == null) return;

                      if (startDate != null) {
                        setState(() {
                          _startDate = startDate;
                          stringStart = _startDate.toString();

                          stringStart = stringStart.substring(8, 10);
                          intStart = int.parse(stringStart);
                        });
                      }
                    }),
                    child: Container(
                      width: 150,
                      height: 30,
                      decoration: BoxDecoration(
                          color: Color(0xff974063),
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Starting Date',
                            style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Icon(
                            FeatherIcons.play,
                            size: 16,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      DateTime? endDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now(),
                        lastDate: DateTime(2025, 12, 10),
                      );

                      if (endDate == null) return;

                      if (endDate != null) {
                        setState(() {
                          _endDate = endDate;
                          stringEnd = _endDate.toString();

                          stringEnd = stringEnd.substring(8, 11);
                          intEnd = int.parse(stringEnd);
                        });
                      }
                    },
                    child: Container(
                      width: 150,
                      height: 30,
                      decoration: BoxDecoration(
                          color: Color(0xff974063),
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Ending Date',
                            style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Icon(
                            FeatherIcons.square,
                            size: 16,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total : ',
                    style: GoogleFonts.montserrat(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        'Rp ',
                        style: GoogleFonts.montserrat(
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Text(
                        totalCount().toString(),
                        style: GoogleFonts.montserrat(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                ],
              ),

              SizedBox(
                height: 80,
              ),

              //Rent Now Button
              GestureDetector(
                onTap: () {
                  if (_startDate != null && _endDate != null) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => orderDetails(
                          pic: widget.pic,
                          totalPrice: totalPrice,
                          brand: widget.brand,
                          type: widget.type,
                          startDate: _startDate,
                          endDate: _endDate,
                          price: widget.price,
                        ),
                      ),
                    );
                  } else {
                    showDialog(
                        context: context,
                        builder: ((context) => AlertDialog(
                              icon: Icon(
                                FeatherIcons.info,
                                color: Colors.redAccent,
                              ),
                              title: Text(
                                'Alert',
                                style: GoogleFonts.montserrat(
                                    fontSize: 24, color: Color(0xff0E0F0E)),
                              ),
                              content: Text(
                                'Please fill rent date!',
                                style: GoogleFonts.montserrat(
                                  fontSize: 12,
                                  color: Color(0xff0E0F0E),
                                ),
                                textAlign: TextAlign.center,
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                            )));
                  }
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 52,
                  decoration: BoxDecoration(
                    color: Color(0xff41436A),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Rent Now",
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                      const Icon(
                        FeatherIcons.chevronRight,
                        color: Colors.white,
                        size: 24,
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  totalCount() {
    if (_startDate == null && _endDate == null) {
      return 0;
    } else if (_startDate == null && _endDate != null) {
      return 0;
    } else if (_startDate != null && _endDate == null) {
      return 0;
    } else {
      total = (intEnd - intStart) + 1;
      totalPrice = widget.price * total;
      return totalPrice;
    }
  }
}
