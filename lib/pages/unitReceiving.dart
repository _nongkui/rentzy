// ignore_for_file: prefer_const_constructors, prefer_interpolation_to_compose_strings, use_build_context_synchronously

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class unitReceiving extends StatefulWidget {
  final dynamic type;
  final dynamic brand;
  final dynamic total;
  final dynamic pic;
  final dynamic price;
  final startingDate;
  final endingDate;

  const unitReceiving({
    super.key,
    required this.brand,
    required this.type,
    required this.pic,
    required this.endingDate,
    required this.startingDate,
    required this.total,
    required this.price,
  });

  @override
  State<unitReceiving> createState() => _unitReceivingState();
}

class _unitReceivingState extends State<unitReceiving> {
  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("Self Pick Up"), value: "Self Pick Up"),
      DropdownMenuItem(child: Text("Delivered"), value: "Delivered"),
    ];
    return menuItems;
  }

  String selectedValue = "Self Pick Up";
  final _addressController = TextEditingController();

  disabledTextfield() {
    if (selectedValue == 'Self Pick Up') {
      return true;
    } else {
      return false;
    }
  }

  addPickUpLoc() async {
    if (selectedValue == 'Delivered') {
      await FirebaseFirestore.instance
          .collection('orders')
          .where('type', isEqualTo: widget.type)
          .where('startDate', isEqualTo: widget.startingDate)
          .where('endDate', isEqualTo: widget.endingDate)
          .get()
          .then(
            (value) => value.docs.forEach((element) {
              FirebaseFirestore.instance
                  .collection('orders')
                  .doc(element.id)
                  .set({'location': _addressController.text.trim()},
                      SetOptions(merge: true));
            }),
          );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 32,
            left: 24,
            right: 24,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      FeatherIcons.chevronLeft,
                      size: 32,
                    ),
                  ),
                  SizedBox(
                    width: 24,
                  ),
                  Text(
                    'Unit Acceptance',
                    style: GoogleFonts.montserrat(
                      fontSize: 32,
                      fontWeight: FontWeight.w500,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 40,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Your Bike',
                        style: GoogleFonts.montserrat(
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Color(0xffD9D9D9),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  bottomLeft: Radius.circular(8)),
                              child: Image.network(
                                widget.pic != ''
                                    ? 'https://drive.google.com/uc?export=view&id=' +
                                        widget.pic
                                    : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                              ),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.type,
                                  style: GoogleFonts.montserrat(
                                    fontSize: 24,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Text(
                                  widget.brand,
                                  style: GoogleFonts.montserrat(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                                SizedBox(
                                  height: 24,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'Rp ',
                                      style: GoogleFonts.montserrat(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Text(
                                      widget.price.toString(),
                                      style: GoogleFonts.montserrat(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Text(
                                      ' / Day',
                                      style: GoogleFonts.montserrat(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'Starting Date',
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 56,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xff41436A)),
                          borderRadius: BorderRadius.circular(8),
                          color: Color(0xffFFFFFF),
                        ),
                        child: Center(
                          child: Text(
                            widget.startingDate.toString().substring(0, 10),
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'Ending Date',
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 56,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xff41436A)),
                          borderRadius: BorderRadius.circular(8),
                          color: Color(0xffFFFFFF),
                        ),
                        child: Center(
                          child: Text(
                            widget.endingDate.toString().substring(0, 10),
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Total',
                            style: GoogleFonts.montserrat(
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Row(
                            children: [
                              Text(
                                'Rp',
                                style: GoogleFonts.montserrat(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Text(
                                widget.total.toString(),
                                style: GoogleFonts.montserrat(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Text(
                        'Choose your payment method to pay the rental fee.',
                        style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 56,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Color(0xff000000)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 8,
                            right: 8,
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              hint: Text(
                                'Select Payment Method!',
                                style: GoogleFonts.montserrat(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w300,
                                    color: Color(
                                      0xff000000,
                                    )),
                              ),
                              style: TextStyle(
                                fontSize: 16,
                                color: Color(
                                  0xff000000,
                                ),
                              ),
                              value: selectedValue,
                              items: dropdownItems,
                              onChanged: (String? newValue) {
                                setState(() {
                                  selectedValue = newValue!;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            border: Border.all(color: Color(0xff41436A)),
                            borderRadius: BorderRadius.circular(8),
                            color: disabledTextfield()
                                ? Colors.grey[350]
                                : Color(0xffFFFFFF)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: TextField(
                            controller: _addressController,
                            enabled:
                                selectedValue == 'Self Pick Up' ? false : true,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Add Your Address',
                              hintStyle: GoogleFonts.montserrat(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: disabledTextfield()
                                      ? Colors.white
                                      : Color(0xff0E0F0E)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Center(
                        child: GestureDetector(
                          onTap: () async {
                            await addPickUpLoc();
                            await FirebaseFirestore.instance
                                .collection('units')
                                .where('type', isEqualTo: widget.type)
                                .get()
                                .then((value) => value.docs.forEach((element) {
                                      FirebaseFirestore.instance
                                          .collection('units')
                                          .doc(element.id)
                                          .update({'available': false});
                                    }));
                            Navigator.pop(context);
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 56,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Color(0xff41436A),
                            ),
                            child: Center(
                              child: Text(
                                'Confirm',
                                style: GoogleFonts.montserrat(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
