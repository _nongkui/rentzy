// ignore_for_file: prefer_const_constructors, prefer_interpolation_to_compose_strings

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/components/historyCard.dart';
import 'package:rentzy_rpl/models/orders.dart';
import 'package:rentzy_rpl/pages/editOrder.dart';
import 'package:rentzy_rpl/pages/paymentMethod.dart';

class waitingForPayment extends StatefulWidget {
  waitingForPayment({super.key});

  @override
  State<waitingForPayment> createState() => _waitingForPaymentState();
}

class _waitingForPaymentState extends State<waitingForPayment> {
  @override
  void didChangeDependencies() {
    getUserOrders();
  }

  @override
  List<Object> _ordersList = [];
  List<Object> _ordersId = [];

  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser!.uid.toString();

    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.only(
          top: 32,
          right: 24,
          left: 24,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    FeatherIcons.chevronLeft,
                    size: 32,
                  ),
                ),
                Text(
                  'Waiting for Payment',
                  style: GoogleFonts.montserrat(
                    fontSize: 28,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(
                  width: 15,
                )
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height - 100,
              child: StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance
                    .collection('orders')
                    .where('status', isEqualTo: 'Waiting for Payment')
                    .where('userId', isEqualTo: user)
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  return ListView(
                    children: snapshot.data!.docs
                        .map((doc) => GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => editOrder(
                                              pic: doc['pic'],
                                              startDate: doc['startDate'],
                                              endDate: doc['endDate'],
                                              type: doc['type'],
                                              brand: doc['brand'],
                                              price: doc['price'],
                                              total: doc['totalPrice'],
                                              id: doc.id,
                                              paymentMethod: doc['method'],
                                              confirmed: doc['confirmed'],
                                            )));
                              },
                              child: Padding(
                                padding: const EdgeInsets.only(top: 8),
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 109,
                                  decoration: BoxDecoration(
                                    color: const Color(0xffD9D9D9),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Row(
                                    children: [
                                      ClipRRect(
                                        borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(8),
                                            bottomLeft: Radius.circular(8)),
                                        child: Image.network(
                                          doc['pic'] != ''
                                              ? 'https://drive.google.com/uc?export=view&id=' +
                                                  doc["pic"]
                                              : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Rent Date: ',
                                            style: GoogleFonts.montserrat(
                                                fontSize: 8,
                                                fontWeight: FontWeight.w700),
                                          ),
                                          Text(
                                            doc['startDate']
                                                    .toString()
                                                    .substring(8, 10) +
                                                '-' +
                                                doc['endDate']
                                                    .toString()
                                                    .substring(8, 10),
                                            style: GoogleFonts.montserrat(
                                                fontSize: 8,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Text(
                                            doc['type'].toString(),
                                            style: GoogleFonts.montserrat(
                                              fontSize: 24,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          Text(
                                            doc["brand"].toString(),
                                            style: GoogleFonts.montserrat(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w300,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 24,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Text(
                                                doc["status"].toString(),
                                                style: GoogleFonts.montserrat(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ))
                        .toList(),
                  );
                },
              ),
            )
          ],
        ),
      )),
    );
  }

  Future getUserOrders() async {
    final users = FirebaseAuth.instance.currentUser?.uid;
    var data = await FirebaseFirestore.instance
        .collection('users')
        .doc(users)
        .collection('orders')
        .where('status', isEqualTo: 'Waiting for Payment')
        .get();

    setState(() {
      _ordersList = List.from(data.docs.map((doc) => Orders.fromSnapshot(doc)));
    });
  }
}
