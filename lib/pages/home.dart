// ignore_for_file: prefer_const_constructors, unnecessary_new, prefer_interpolation_to_compose_strings, sized_box_for_whitespace
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/pages/history.dart';
import 'package:rentzy_rpl/pages/profile.dart';
import 'package:rentzy_rpl/pages/unit.dart';

class Home extends StatefulWidget {
  Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var user = FirebaseAuth.instance.currentUser!;
  dynamic db;
  bool available = true;

  @override
  Widget build(BuildContext context) {
    String searchKey;

    return Scaffold(
      backgroundColor: Color(0xffFFFFFF),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  top: 32,
                  right: 24,
                  left: 24,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        //Avatar
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Profile(),
                              ),
                            );
                          },
                          child: Container(
                            child: CircleAvatar(
                              backgroundImage:
                                  AssetImage('assets/avatar-1.jpg'),
                              radius: 22,
                            ),
                          ),
                        ),

                        //History Icon
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => History(),
                              ),
                            );
                          },
                          child: Icon(
                            FeatherIcons.clock,
                            size: 32,
                          ),
                        )
                      ],
                    ),

                    SizedBox(
                      height: 32,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Hi ' + user.displayName.toString() + '!',
                          style: GoogleFonts.montserrat(
                            fontSize: 30,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 8,
                    ),

                    //Text Title
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xff974063),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 24,
                          horizontal: 14,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Find the Best ",
                                          style: GoogleFonts.montserrat(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xffffffff),
                                          ),
                                        ),
                                        Text(
                                          "Bike",
                                          style: GoogleFonts.montserrat(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xffFF9677),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      "to Rent Today!",
                                      style: GoogleFonts.montserrat(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xffffffff),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 32,
                                ),
                                SvgPicture.asset(
                                  'assets/homecard.svg',
                                  width: 101,
                                  height: 78,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    //Search Bar
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xffD9D9D9),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 2.5, 0, 2.5),
                        child: TextField(
                          onSubmitted: (value) {
                            if (available == true) {
                              setState(() {
                                searchKey = value;
                                db = FirebaseFirestore.instance
                                    .collection('units')
                                    .where('brand', isEqualTo: searchKey)
                                    .where('available', isEqualTo: true)
                                    .snapshots();
                              });
                            } else {
                              setState(() {
                                searchKey = value;
                                db = FirebaseFirestore.instance
                                    .collection('units')
                                    .where('brand', isEqualTo: searchKey)
                                    .snapshots();
                              });
                            }
                          },
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Search",
                            hintStyle: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                              color: const Color(0xff0E0F0E),
                            ),
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 24,
                    ),

                    //Button
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        //Available
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              available = true;
                              db = FirebaseFirestore.instance
                                  .collection('units')
                                  .where('available', isEqualTo: true)
                                  .snapshots();
                            });
                          },
                          child: Container(
                            width: 154,
                            height: 56,
                            decoration: BoxDecoration(
                              color:
                                  available ? Color(0xff41436A) : Colors.white,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Available",
                                  style: GoogleFonts.montserrat(
                                      fontSize: 16,
                                      color: available
                                          ? Color(0xffffffff)
                                          : Color(0xff1E1E1E),
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          ),
                        ),
                        //All
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              available = false;
                              db = FirebaseFirestore.instance
                                  .collection('units')
                                  .snapshots();
                            });
                          },
                          child: Container(
                            width: 154,
                            height: 56,
                            decoration: BoxDecoration(
                              color: available
                                  ? Color(0xffffffff)
                                  : Color(0xff41436A),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "All",
                                  style: GoogleFonts.montserrat(
                                      fontSize: 16,
                                      color: available
                                          ? Color(0xff1E1E1E)
                                          : Colors.white,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 16,
                    ),

                    //Units List
                    Container(
                      height: 350,
                      width: MediaQuery.of(context).size.width,
                      child: StreamBuilder<QuerySnapshot>(
                        stream: db,
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          return ListView(
                            children: snapshot.data!.docs
                                .map((doc) => Padding(
                                      padding: const EdgeInsets.only(bottom: 8),
                                      child: GestureDetector(
                                        onTap: () => Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Units(
                                                    details: doc,
                                                  )),
                                        ),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Color(0xffD9D9D9),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: Row(
                                            children: [
                                              ClipRRect(
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(8),
                                                    bottomLeft:
                                                        Radius.circular(8)),
                                                child: Image.network(
                                                  doc['pic'] != ''
                                                      ? 'https://drive.google.com/uc?export=view&id=' +
                                                          doc["pic"]
                                                      : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    doc['type'],
                                                    style:
                                                        GoogleFonts.montserrat(
                                                      fontSize: 24,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                  Text(
                                                    doc['brand'],
                                                    style:
                                                        GoogleFonts.montserrat(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w300,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 24,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        'Rp ',
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                      Text(
                                                        doc['price'].toString(),
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                      Text(
                                                        ' / Day',
                                                        style: GoogleFonts
                                                            .montserrat(
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ))
                                .toList(),
                          );
                        },
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
