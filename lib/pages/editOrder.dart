// ignore_for_file: prefer_const_constructors, await_only_futures, use_build_context_synchronously, prefer_interpolation_to_compose_strings

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/main.dart';
import 'package:rentzy_rpl/models/orders.dart';
import 'package:rentzy_rpl/pages/paymentMethod.dart';
import 'package:rentzy_rpl/pages/unitReceiving.dart';

class editOrder extends StatefulWidget {
  final pic;
  dynamic startDate;
  dynamic endDate;
  final type;
  final brand;
  final price;
  dynamic total;
  final id;
  String paymentMethod;
  bool confirmed;

  editOrder(
      {super.key,
      required this.pic,
      required this.startDate,
      required this.endDate,
      required this.type,
      required this.brand,
      required this.price,
      required this.total,
      required this.id,
      required this.paymentMethod,
      required this.confirmed});

  @override
  State<editOrder> createState() => _editOrderState();
}

class _editOrderState extends State<editOrder> {
  var _startDate;
  var _endDate;
  var count = 0;
  var stringStart;
  var stringEnd;
  var intStart;
  var intEnd;
  var totalPrice;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.only(
          top: 32,
          left: 24,
          right: 24,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Icon(
                    FeatherIcons.chevronLeft,
                    size: 32,
                    color: Color(0xff1E1E1E),
                  ),
                ),
                const SizedBox(
                  width: 68,
                ),
                Text(
                  'Edit Order',
                  style: GoogleFonts.montserrat(
                    fontSize: 32,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff1E1E1E),
                  ),
                ),
              ],
            ),

            const SizedBox(
              height: 24,
            ),

            //Unit Info
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Color(0xffD9D9D9),
                borderRadius: BorderRadius.circular(8),
              ),
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Image.network(
                      widget.pic != ''
                          ? 'https://drive.google.com/uc?export=view&id=' +
                              widget.pic
                          : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.type,
                        style: GoogleFonts.montserrat(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Text(
                        widget.brand,
                        style: GoogleFonts.montserrat(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: [
                          Text(
                            'Rp ',
                            style: GoogleFonts.montserrat(
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            widget.price.toString(),
                            style: GoogleFonts.montserrat(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            ' / Day',
                            style: GoogleFonts.montserrat(
                              fontSize: 20,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),

            const SizedBox(
              height: 16,
            ),

            Text(
              'Starting Date: ',
              style: GoogleFonts.montserrat(
                fontSize: 16,
                fontWeight: FontWeight.w300,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff41436A)),
                borderRadius: BorderRadius.circular(8),
                color: Color(0xffFFFFFF),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      newstarDate().toString().substring(0, 10),
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        DateTime? newstartDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime.now(),
                          lastDate: DateTime(2025, 12, 10),
                        );

                        if (newstartDate == null) return;

                        if (newstartDate != null) {
                          setState(() {
                            _startDate = newstartDate.toString();
                            stringStart = _startDate.toString();

                            stringStart = stringStart.substring(8, 11);
                            intStart = int.parse(stringStart);
                          });
                        }
                      },
                      child: Icon(
                        FeatherIcons.calendar,
                        size: 16,
                      ),
                    )
                  ],
                ),
              ),
            ),

            const SizedBox(
              height: 16,
            ),

            //Ending Date
            Text(
              'Ending Date: ',
              style: GoogleFonts.montserrat(
                fontSize: 16,
                fontWeight: FontWeight.w300,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff41436A)),
                borderRadius: BorderRadius.circular(8),
                color: Color(0xffFFFFFF),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                  right: 8,
                  left: 8,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      newendDate().toString().substring(0, 10),
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        DateTime? newendDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime.now(),
                          lastDate: DateTime(2025, 12, 10),
                        );

                        if (newendDate == null) return;

                        if (newendDate != null) {
                          setState(() {
                            _endDate = newendDate.toString();
                            stringEnd = _endDate.toString();

                            stringEnd = stringEnd.substring(8, 11);
                            intEnd = int.parse(stringEnd);
                          });
                        }
                      },
                      child: Icon(
                        FeatherIcons.calendar,
                        size: 16,
                      ),
                    )
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 24,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total: ',
                  style: GoogleFonts.montserrat(
                    fontSize: 24,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Row(
                  children: [
                    Text(
                      'Rp ',
                      style: GoogleFonts.montserrat(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(
                      totalCount().toString(),
                      style: GoogleFonts.montserrat(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                )
              ],
            ),

            SizedBox(
              height: 64,
            ),

            GestureDetector(
              onTap: () async {
                if (widget.paymentMethod == 'Bank Transfer') {
                  await updateData();
                  await Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => paymentMethod(
                          type: widget.type,
                          brand: widget.brand,
                          total: widget.total,
                          startingDate: widget.startDate,
                          endingDate: widget.endDate,
                          pic: widget.pic,
                          price: widget.price,
                          id: widget.id,
                          confirmed: widget.confirmed,
                        ),
                      ));
                } else {
                  await updateData();
                  await updateStatus();
                  await Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => unitReceiving(
                              brand: widget.brand,
                              type: widget.type,
                              pic: widget.pic,
                              endingDate: widget.endDate,
                              startingDate: widget.startDate,
                              total: widget.total,
                              price: widget.price)));
                }
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 52,
                decoration: BoxDecoration(
                  color: Color(0xff41436A),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Pay",
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    const Icon(
                      FeatherIcons.chevronRight,
                      color: Colors.white,
                      size: 20,
                    )
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 16,
            ),

            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: ((context) => AlertDialog(
                          title: Center(
                            child: Text(
                              'Are you sure?',
                              style: GoogleFonts.montserrat(
                                fontSize: 20,
                              ),
                            ),
                          ),
                          icon: Icon(
                            FeatherIcons.info,
                            color: Colors.redAccent,
                          ),
                          actions: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text(
                                        'Cancel',
                                        style: GoogleFonts.montserrat(
                                          fontSize: 12,
                                          color: Colors.black87,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      )),
                                ),
                                Expanded(
                                  child: TextButton(
                                      onPressed: () {
                                        deleteOrders();
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      },
                                      child: Text(
                                        'Delete',
                                        style: GoogleFonts.montserrat(
                                          fontSize: 12,
                                          color: Colors.redAccent,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      )),
                                )
                              ],
                            ),
                          ],
                        )));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 52,
                decoration: BoxDecoration(
                  color: Colors.redAccent,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Cancel Order",
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    const Icon(
                      FeatherIcons.trash2,
                      color: Colors.white,
                      size: 20,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }

  totalCount() {
    if (_startDate == null && _endDate == null) {
      return widget.total;
    } else if (_startDate == null && _endDate != null) {
      return widget.total;
    } else if (_startDate != null && _endDate == null) {
      return widget.total;
    } else {
      count = (intEnd - intStart) + 1;
      totalPrice = widget.price * count;
      setState(() {
        widget.total = totalPrice;
      });
      return widget.total;
    }
  }

  updateData() async {
    await FirebaseFirestore.instance
        .collection('orders')
        .doc(widget.id)
        .update({
      "startDate": newstarDate(),
      "endDate": newendDate(),
      "totalPrice": widget.total,
    }).then((value) => print("Sucessfully updated"), onError: (e) => print(e));
  }

  deleteOrders() async {
    await FirebaseFirestore.instance
        .collection('orders')
        .doc(widget.id)
        .delete()
        .then((value) => print('Orders succesfully deleted'),
            onError: (e) => print(e));
  }

  newstarDate() {
    if (_startDate == null) {
      return widget.startDate;
    } else {
      return _startDate;
    }
  }

  newendDate() {
    if (_endDate == null) {
      return widget.endDate;
    } else {
      return _endDate;
    }
  }

  updateStatus() async {
    await FirebaseFirestore.instance.collection('orders').doc(widget.id).update(
        {'status': 'On Going'}).then((value) => print('Status Updated'));
  }
}
