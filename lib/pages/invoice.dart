// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, prefer_interpolation_to_compose_strings, unused_element

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/components/separator.dart';
import 'package:barcode/barcode.dart';

class Invoice extends StatefulWidget {
  dynamic brand;
  dynamic type;
  dynamic totalPrice;
  dynamic pic;
  dynamic startDate;
  dynamic endDate;
  dynamic userId;
  Invoice(
      {super.key,
      required this.brand,
      required this.type,
      required this.totalPrice,
      required this.pic,
      required this.startDate,
      required this.endDate,
      required this.userId});

  @override
  State<Invoice> createState() => _InvoiceState();
}

class _InvoiceState extends State<Invoice> {
  dynamic orderId;

  @override
  Widget build(BuildContext context) {
    var strStartMonth = widget.startDate.toString().substring(5, 7);
    var startDay = widget.startDate.toString().substring(8, 10);
    var startYear = widget.startDate.toString().substring(0, 4);
    var startMonth;
    int intStartMonth = int.parse(strStartMonth);
    var _startDate;
    switch (intStartMonth) {
      case 1:
        {
          setState(() {
            startMonth = 'January';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 2:
        {
          setState(() {
            startMonth = 'February';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 3:
        {
          setState(() {
            startMonth = 'March';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 4:
        {
          setState(() {
            startMonth = 'April';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 5:
        {
          setState(() {
            startMonth = 'May';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 6:
        {
          setState(() {
            startMonth = 'June';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 7:
        {
          setState(() {
            startMonth = 'July';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 8:
        {
          setState(() {
            startMonth = 'August';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 9:
        {
          setState(() {
            startMonth = 'September';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 10:
        {
          setState(() {
            startMonth = 'October';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 11:
        {
          setState(() {
            startMonth = 'November';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 12:
        {
          setState(() {
            startMonth = 'Desember';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      default:
    }

    var strEndMonth = widget.endDate.toString().substring(5, 7);
    var endDay = widget.endDate.toString().substring(8, 10);
    var endYear = widget.endDate.toString().substring(0, 4);
    var endMonth;
    int intEndMonth = int.parse(strStartMonth);
    var _endDate;
    switch (intEndMonth) {
      case 1:
        {
          setState(() {
            endMonth = 'January';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 2:
        {
          setState(() {
            endMonth = 'February';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 3:
        {
          setState(() {
            endMonth = 'March';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 4:
        {
          setState(() {
            endMonth = 'April';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 5:
        {
          setState(() {
            endMonth = 'May';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 6:
        {
          setState(() {
            endMonth = 'June';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 7:
        {
          setState(() {
            endMonth = 'July';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 8:
        {
          setState(() {
            endMonth = 'August';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 9:
        {
          setState(() {
            endMonth = 'September';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 10:
        {
          setState(() {
            endMonth = 'October';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 11:
        {
          setState(() {
            endMonth = 'November';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 12:
        {
          setState(() {
            endMonth = 'Desember';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      default:
    }

    FirebaseFirestore.instance
        .collection('orders')
        .where('userId', isEqualTo: widget.userId)
        .where('type', isEqualTo: widget.type)
        .where('startDate', isEqualTo: widget.startDate)
        .where('endDate', isEqualTo: widget.endDate)
        .get()
        .then((value) => value.docs.forEach((element) {
              print(element.id);
              if (mounted) {
                setState(() {
                  orderId = element.id;
                });
              }
            }));

    final qr = Barcode.fromType(BarcodeType.QrCode).toSvg(orderId);

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: AppBar(
          forceMaterialTransparency: true,
          centerTitle: true,
          title: Text(
            'Order Summary',
            style: GoogleFonts.montserrat(
                fontSize: 32,
                fontWeight: FontWeight.w500,
                color: Color(0xff0E0F0E)),
          ),
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              FeatherIcons.chevronLeft,
              color: Color(0xff0E0F0E),
              size: 32,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
      ),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 32,
          horizontal: 24,
        ),
        child: Column(
          children: [
            Text(
              'Order Number:',
              style: GoogleFonts.montserrat(
                fontSize: 24,
                fontWeight: FontWeight.w500,
              ),
            ),
            Text(orderId.toString(),
                style: GoogleFonts.montserrat(
                    fontSize: 16, fontWeight: FontWeight.w300)),
            SizedBox(
              height: 8,
            ),
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Color.fromARGB(255, 235, 235, 235),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                      ),
                      child: Image.network(
                        widget.pic != ''
                            ? 'https://drive.google.com/uc?export=view&id=' +
                                widget.pic
                            : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                        scale: 0.3,
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          'Rent Start Date',
                          style: GoogleFonts.montserrat(
                              fontSize: 16, fontWeight: FontWeight.w500),
                        ),
                        Text(
                          _startDate,
                          style: GoogleFonts.montserrat(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          'Rent End Date',
                          style: GoogleFonts.montserrat(
                              fontSize: 16, fontWeight: FontWeight.w500),
                        ),
                        Text(
                          _endDate,
                          style: GoogleFonts.montserrat(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      'Total',
                      style: GoogleFonts.montserrat(
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Rp ',
                          style: GoogleFonts.montserrat(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(
                          widget.totalPrice.toString(),
                          style: GoogleFonts.montserrat(
                              fontSize: 30, fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 24, left: 16, right: 16, bottom: 8),
                      child: Separator(
                        height: 3,
                        color: Colors.grey,
                      ),
                    ),
                    SvgPicture.string(
                      qr,
                      width: 150,
                      height: 150,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
