// ignore_for_file: prefer_const_constructors, unused_local_variable, use_build_context_synchronously
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/pages/history.dart';
import 'package:rentzy_rpl/pages/unitReceiving.dart';
import 'package:rentzy_rpl/pages/waitingforpayment.dart';

class paymentMethod extends StatefulWidget {
  final dynamic type;
  final dynamic brand;
  final dynamic total;
  final dynamic pic;
  final dynamic startingDate;
  final dynamic endingDate;
  final dynamic price;
  final dynamic id;
  bool confirmed;
  paymentMethod({
    super.key,
    required this.brand,
    required this.type,
    required this.pic,
    required this.total,
    required this.startingDate,
    required this.endingDate,
    required this.price,
    required this.id,
    required this.confirmed,
  });

  @override
  State<paymentMethod> createState() => _paymentMethodState();
}

class _paymentMethodState extends State<paymentMethod> {
  var _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.only(
          top: 32,
          right: 24,
          left: 24,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    FeatherIcons.chevronLeft,
                    size: 32,
                  ),
                ),
                Text(
                  'Payment Details',
                  style: GoogleFonts.montserrat(
                    fontSize: 32,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(
                  width: 20,
                )
              ],
            ),
            SizedBox(
              height: 32,
            ),
            Image.asset(
              'assets/payment.png',
              width: 211,
              height: 114,
            ),
            SizedBox(
              height: 81,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Payment Code',
                  style: GoogleFonts.montserrat(
                    fontSize: 36,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  'Copy and paste the code below to a payment platform of your choice, or give the code to rental the owner.',
                  style: GoogleFonts.montserrat(
                    fontSize: 12,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 64),
                    child: Text(
                      getRandomString(8),
                      style: GoogleFonts.montserrat(
                        fontSize: 32,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 64,
            ),
            GestureDetector(
              onTap: () async {
                if (widget.confirmed == true) {
                  await updateStatus();
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => unitReceiving(
                                type: widget.type,
                                brand: widget.brand,
                                total: widget.total,
                                startingDate: widget.startingDate,
                                endingDate: widget.endingDate,
                                pic: widget.pic,
                                price: widget.price,
                              )));
                }
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 56,
                decoration: BoxDecoration(
                  color: Color(0xff41436A),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Center(
                  child: Text(
                    'Continue',
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 24,
            ),
            GestureDetector(
              onTap: () async {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => History()));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 56,
                decoration: BoxDecoration(
                  color: Color(0xffffffff),
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                    color: Color(0xff41436A),
                    width: 2,
                  ),
                ),
                child: Center(
                  child: Text(
                    'Payment Status',
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: Color(0xff41436A),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      )),
    );
  }

  updateStatus() async {
    await FirebaseFirestore.instance.collection('orders').doc(widget.id).update(
        {'status': 'On Going'}).then((value) => print('Status Updated'));
  }
}
