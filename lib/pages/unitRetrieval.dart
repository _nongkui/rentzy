// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, prefer_interpolation_to_compose_strings, use_build_context_synchronously
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class unitRetrieval extends StatefulWidget {
  final dynamic type;
  final dynamic brand;
  final dynamic total;
  final dynamic pic;
  final dynamic price;
  final startingDate;
  final endingDate;
  const unitRetrieval({
    super.key,
    required this.brand,
    required this.type,
    required this.pic,
    required this.endingDate,
    required this.startingDate,
    required this.total,
    required this.price,
  });

  @override
  State<unitRetrieval> createState() => _unitRetrievalState();
}

class _unitRetrievalState extends State<unitRetrieval> {
  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("Self Return"), value: "Self Return"),
      DropdownMenuItem(child: Text("Picked Up"), value: "Picked Up"),
    ];
    return menuItems;
  }

  String selectedValue = "Self Return";
  final _addressController = TextEditingController();

  disabledTextfield() {
    if (selectedValue == 'Self Return') {
      return false;
    } else {
      return true;
    }
  }

  addAddress() {
    if (selectedValue == 'Picked Up') {
      print(_addressController.text.trim());
      FirebaseFirestore.instance
          .collection('orders')
          .where('type', isEqualTo: widget.type)
          .where('startDate', isEqualTo: widget.startingDate)
          .where('endDate', isEqualTo: widget.endingDate)
          .get()
          .then(
            (value) => value.docs.forEach((element) {
              FirebaseFirestore.instance
                  .collection('orders')
                  .doc(element.id)
                  .update({'location': _addressController.text.trim()});
            }),
          );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.only(
          top: 32,
          left: 24,
          right: 24,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    FeatherIcons.chevronLeft,
                    size: 32,
                  ),
                ),
                SizedBox(
                  width: 48,
                ),
                Text(
                  'Unit Retrieval',
                  style: GoogleFonts.montserrat(
                    fontSize: 32,
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Your Bike',
                      style: GoogleFonts.montserrat(
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xffD9D9D9),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8),
                                bottomLeft: Radius.circular(8)),
                            child: Image.network(
                              widget.pic != ''
                                  ? 'https://drive.google.com/uc?export=view&id=' +
                                      widget.pic
                                  : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.type,
                                style: GoogleFonts.montserrat(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Text(
                                widget.brand,
                                style: GoogleFonts.montserrat(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                              SizedBox(
                                height: 24,
                              ),
                              Row(
                                children: [
                                  Text(
                                    'Rp ',
                                    style: GoogleFonts.montserrat(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  Text(
                                    widget.price.toString(),
                                    style: GoogleFonts.montserrat(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  Text(
                                    ' / Day',
                                    style: GoogleFonts.montserrat(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      'Starting Date',
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 56,
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xff41436A)),
                        borderRadius: BorderRadius.circular(8),
                        color: Color(0xffFFFFFF),
                      ),
                      child: Center(
                        child: Text(
                          widget.startingDate.toString().substring(0, 10),
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      'Ending Date',
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 56,
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xff41436A)),
                        borderRadius: BorderRadius.circular(8),
                        color: Color(0xffFFFFFF),
                      ),
                      child: Center(
                        child: Text(
                          widget.endingDate.toString().substring(0, 10),
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Total',
                          style: GoogleFonts.montserrat(
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Row(
                          children: [
                            Text(
                              'Rp',
                              style: GoogleFonts.montserrat(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              widget.total.toString(),
                              style: GoogleFonts.montserrat(
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Text(
                      'Choose your payment method to pay the rental fee.',
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 56,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: Color(0xff000000)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 8,
                          right: 8,
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            hint: Text(
                              'Select Payment Method!',
                              style: GoogleFonts.montserrat(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w300,
                                  color: Color(
                                    0xff000000,
                                  )),
                            ),
                            style: TextStyle(
                              fontSize: 16,
                              color: Color(
                                0xff000000,
                              ),
                            ),
                            value: selectedValue,
                            items: dropdownItems,
                            onChanged: (String? newValue) {
                              setState(() {
                                selectedValue = newValue!;
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(0xff41436A)),
                          borderRadius: BorderRadius.circular(8),
                          color: disabledTextfield()
                              ? Colors.white
                              : Colors.grey[350]),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: TextField(
                          controller: _addressController,
                          enabled:
                              selectedValue == 'Self Return' ? false : true,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Add Your Address',
                            hintStyle: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: disabledTextfield()
                                    ? Color(0xff0E0F0E)
                                    : Colors.white),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Center(
                      child: GestureDetector(
                        onTap: () async {
                          await addAddress();
                          await FirebaseFirestore.instance
                              .collection('orders')
                              .where('type', isEqualTo: widget.type)
                              .where('startDate',
                                  isEqualTo: widget.startingDate)
                              .where('endDate', isEqualTo: widget.endingDate)
                              .get()
                              .then((value) =>
                                  value.docs.forEach((element) async {
                                    if (selectedValue != 'Picked Up') {
                                      await FirebaseFirestore.instance
                                          .collection('orders')
                                          .doc(element.id)
                                          .update({'status': 'Done'});
                                    } else {
                                      await FirebaseFirestore.instance
                                          .collection('orders')
                                          .doc(element.id)
                                          .update({
                                        'status': 'Waiting for Pick Up'
                                      });
                                    }
                                  }));
                          await FirebaseFirestore.instance
                              .collection('units')
                              .where('type', isEqualTo: widget.type)
                              .get()
                              .then(
                            (value) {
                              value.docs.forEach((element) async {
                                await FirebaseFirestore.instance
                                    .collection('units')
                                    .doc(element.id)
                                    .update({'available': true});
                              });
                            },
                          );
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 56,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xff41436A),
                          ),
                          child: Center(
                            child: Text(
                              'Confirm',
                              style: GoogleFonts.montserrat(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
