// ignore_for_file: prefer_const_constructors, prefer_interpolation_to_compose_strings

import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/pages/paymentDetails.dart';

class orderDetails extends StatefulWidget {
  final int totalPrice;
  final dynamic pic;
  final dynamic type;
  final dynamic brand;
  final dynamic price;
  final DateTime? startDate;
  final DateTime? endDate;
  const orderDetails({
    super.key,
    required this.pic,
    required this.price,
    required this.totalPrice,
    required this.brand,
    required this.type,
    required this.startDate,
    required this.endDate,
  });

  @override
  State<orderDetails> createState() => _orderDetailsState();
}

class _orderDetailsState extends State<orderDetails> {
  @override
  Widget build(BuildContext context) {
    var strStartMonth = widget.startDate.toString().substring(5, 7);
    var startDay = widget.startDate.toString().substring(8, 10);
    var startYear = widget.startDate.toString().substring(0, 4);
    var startMonth;
    int intStartMonth = int.parse(strStartMonth);
    var _startDate;
    switch (intStartMonth) {
      case 1:
        {
          setState(() {
            startMonth = 'January';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 2:
        {
          setState(() {
            startMonth = 'February';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 3:
        {
          setState(() {
            startMonth = 'March';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 4:
        {
          setState(() {
            startMonth = 'April';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 5:
        {
          setState(() {
            startMonth = 'May';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 6:
        {
          setState(() {
            startMonth = 'June';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 7:
        {
          setState(() {
            startMonth = 'July';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 8:
        {
          setState(() {
            startMonth = 'August';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 9:
        {
          setState(() {
            startMonth = 'September';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 10:
        {
          setState(() {
            startMonth = 'October';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 11:
        {
          setState(() {
            startMonth = 'November';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      case 12:
        {
          setState(() {
            startMonth = 'December';
            _startDate = startDay + ' ' + startMonth + " " + startYear;
          });
        }
        break;
      default:
    }

    var strEndMonth = widget.endDate.toString().substring(5, 7);
    var endDay = widget.endDate.toString().substring(8, 10);
    var endYear = widget.endDate.toString().substring(0, 4);
    var endMonth;
    int intEndMonth = int.parse(strStartMonth);
    var _endDate;
    switch (intEndMonth) {
      case 1:
        {
          setState(() {
            endMonth = 'January';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 2:
        {
          setState(() {
            endMonth = 'February';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 3:
        {
          setState(() {
            endMonth = 'March';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 4:
        {
          setState(() {
            endMonth = 'April';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 5:
        {
          setState(() {
            endMonth = 'May';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 6:
        {
          setState(() {
            endMonth = 'June';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 7:
        {
          setState(() {
            endMonth = 'July';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 8:
        {
          setState(() {
            endMonth = 'August';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 9:
        {
          setState(() {
            endMonth = 'September';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 10:
        {
          setState(() {
            endMonth = 'October';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 11:
        {
          setState(() {
            endMonth = 'November';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      case 12:
        {
          setState(() {
            endMonth = 'December';
            _endDate = endDay + ' ' + endMonth + " " + endYear;
          });
        }
        break;
      default:
    }

    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.only(
          left: 24,
          top: 32,
          right: 24,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: const Icon(
                    FeatherIcons.chevronLeft,
                    size: 32,
                    color: Color(0xff1E1E1E),
                  ),
                ),
                const SizedBox(
                  width: 48,
                ),
                Text(
                  'Order Details',
                  style: GoogleFonts.montserrat(
                    fontSize: 32,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff1E1E1E),
                  ),
                ),
              ],
            ),

            const SizedBox(
              height: 16,
            ),

            Container(
              decoration: BoxDecoration(
                color: Color(0xffD9D9D9),
                borderRadius: BorderRadius.circular(8),
              ),
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Image.network(
                      widget.pic != ''
                          ? 'https://drive.google.com/uc?export=view&id=' +
                              widget.pic
                          : 'https://drive.google.com/uc?export=view&id=12BjSc2P4mw3Sic0n7g0LWc8piH-h8vni',
                      scale: 0.3,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 4,
                      top: 8,
                      bottom: 8,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.type,
                          style: GoogleFonts.montserrat(
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(
                          widget.brand,
                          style: GoogleFonts.montserrat(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: [
                            Text(
                              'Rp ',
                              style: GoogleFonts.montserrat(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              widget.price.toString(),
                              style: GoogleFonts.montserrat(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              ' / Day',
                              style: GoogleFonts.montserrat(
                                fontSize: 20,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            const SizedBox(
              height: 16,
            ),

            Text(
              'Starting Date: ',
              style: GoogleFonts.montserrat(
                fontSize: 16,
                fontWeight: FontWeight.w300,
                color: Color(0xff1E1E1E),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff41436A)),
                borderRadius: BorderRadius.circular(8),
                color: Color(0xffFFFFFF),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      _startDate,
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),

            //Ending Date
            Text(
              'Ending Date: ',
              style: GoogleFonts.montserrat(
                fontSize: 16,
                fontWeight: FontWeight.w300,
                color: Color(0xff1E1E1E),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff41436A)),
                borderRadius: BorderRadius.circular(8),
                color: Color(0xffFFFFFF),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      _endDate,
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(
              height: 24,
            ),

            //Total Price
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total',
                  style: GoogleFonts.montserrat(
                    fontSize: 24,
                    fontWeight: FontWeight.w700,
                    color: Color(0xff1E1E1E),
                  ),
                ),
                Row(
                  children: [
                    Text(
                      'Rp ',
                      style: GoogleFonts.montserrat(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                        color: Color(0xff1E1E1E),
                      ),
                    ),
                    Text(
                      widget.totalPrice.toString(),
                      style: GoogleFonts.montserrat(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: Color(0xff1E1E1E),
                      ),
                    ),
                  ],
                ),
              ],
            ),

            SizedBox(
              height: 24,
            ),

            //Pay Button
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => paymentDetails(
                              pic: widget.pic,
                              type: widget.type,
                              brand: widget.brand,
                              price: widget.price,
                              startDate: widget.startDate,
                              endDate: widget.endDate,
                              total: widget.totalPrice,
                            )));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 52,
                decoration: BoxDecoration(
                  color: Color(0xff41436A),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Pay",
                      style: GoogleFonts.montserrat(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                    const Icon(
                      FeatherIcons.chevronRight,
                      color: Colors.white,
                      size: 24,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
