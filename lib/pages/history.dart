// ignore_for_file: prefer_const_constructors
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rentzy_rpl/auth/main_page.dart';
import 'package:rentzy_rpl/components/historyCard.dart';
import 'package:rentzy_rpl/models/orders.dart';
import 'package:rentzy_rpl/pages/waitingforpayment.dart';

class History extends StatefulWidget {
  History({super.key});

  @override
  State<History> createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  void didChangeDependencies() {
    getUserOrders();
  }

  @override
  List<Object> _ordersList = [];

  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 32,
            right: 24,
            left: 24,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: const Icon(
                      FeatherIcons.chevronLeft,
                      size: 32,
                    ),
                  ),
                  Text(
                    "History",
                    style: GoogleFonts.montserrat(
                      fontSize: 32,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    width: 40,
                  )
                ],
              ),
              const SizedBox(
                height: 52,
              ),

              //Order History
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => waitingForPayment()));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 56,
                  decoration: BoxDecoration(
                    color: const Color(0xff41436A),
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 12,
                    ),
                    child: Row(
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        // ignore: prefer_const_constructors
                        Icon(
                          FeatherIcons.info,
                          size: 26,
                          color: const Color(0xffffffff),
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Text(
                          'Waiting for Payment',
                          style: GoogleFonts.montserrat(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                        Spacer(
                          flex: 2,
                        ),
                        Icon(
                          FeatherIcons.chevronRight,
                          size: 26,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height - 230,
                child: ListView.builder(
                  itemCount: _ordersList.length,
                  itemBuilder: (context, index) {
                    return historyCard(_ordersList[index] as Orders);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }

  Future getUserOrders() async {
    dynamic userId = FirebaseAuth.instance.currentUser?.uid.toString();
    var data = await FirebaseFirestore.instance
        .collection('orders')
        .where('userId', isEqualTo: userId)
        .where('status', isNotEqualTo: 'Waiting for Payment')
        .get();

    setState(() {
      _ordersList = List.from(data.docs.map((doc) => Orders.fromSnapshot(doc)));
    });
  }
}
